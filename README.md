# m3-wordle_arnaubadenas
 ## Introducció
Aquest projecte consta d'una reconstrucció del popular joc "Wordle" fet amb Kotlin i utilitzant la terminal com a interfície gràfica.

## Com jugar?
El joc es tracta de encertar una paraula de 5 lletres escollida aleatòriament d'aquest [diccionari](./diccionari.txt).
~~~
---- Wordle! ----
▢ ▢ ▢ ▢ ▢ 
▢ ▢ ▢ ▢ ▢ 
▢ ▢ ▢ ▢ ▢ 
▢ ▢ ▢ ▢ ▢ 
▢ ▢ ▢ ▢ ▢ 
▢ ▢ ▢ ▢ ▢ 
-----------------
Introduce una palabra
~~~
Després de introduir una paraula, es mostra quines lletres NO están a la paraula en <span style="color:red">vermell</span>, les que están a la paraula pero no en el lloc correcte en <span style="color:yellow">groc</span> i les que están bé en <span style="color:green">verd</span>.

La interfície final quedaria així:

<img src="./wordle.png">

## Conclusions
Gràcies per jugar!

<span style="text-align:right">Fet per Arnau Badenas</span>
