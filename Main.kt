import java.util.Scanner
import kotlin.random.Random
fun main() {
    val reader = Scanner(System.`in`)
    var intents = 1
    val taula = arrayOf(
        arrayOf("▢", "▢", "▢", "▢", "▢"),
        arrayOf("▢", "▢", "▢", "▢", "▢"),
        arrayOf("▢", "▢", "▢", "▢", "▢"),
        arrayOf("▢", "▢", "▢", "▢", "▢"),
        arrayOf("▢", "▢", "▢", "▢", "▢"),
        arrayOf("▢", "▢", "▢", "▢", "▢"),
    )
    val groc = "\u001b[33m"
    val vermell = "\u001b[31m"
    val verd = "\u001b[32m"
    val reset = "\u001b[0m"
    //Escollir palabra aleatoria de diccionari
    val diccionari = arrayOf("apoyo","apoya","ansia","amigo","armar","antes","ahora","abrir","audaz","anodo","anulo","anuda","anual","antro","atlas","animo","anejo","anden","andar","anime","anida","anexa","anglo","angel","anexo","apice","apios","apilo","apolo","apodo","aovar","aorta","apano","apaga","apelo","apego","apago","andan","aluza","alzas","alzar","amana","aludo","aludi","alzos","amago","amaba","aloes","almos","altos","altas","altar","amasa","ancas","ancla","ancho","anana","ambos","amena","ambas","anudo","almas","asoma","asear","ascua","ataud","astro","asumo","atajo","artes","ardan","arcos","arcas","arbol","ardor","apuro","apure","arabe","aquel","arena","aroma","armar","arroz","arepa","alfil","acido","achis","acabo","abuso","acata","adios","actuo","acuse","abono","abaco","abeto","alamo","aisla","ajeno","algun","alino","alias","alfil","album","aleta","aleli","airea","adule","afina","afila","aforo","aereo","adulo","aguzo","ahogo","agape","agota","agita","agraz","agudo","agria","babas","biche","babel","berro","brote","besos","bacan","bacas","barro","bases","beber","basta","bongo","bache","brisa","badea","bolsa","bolso","broma","badil","bafle","breve","bahia","becas","baile","belga","bajar","biela","bajio","blusa","bajos","bizco","blues","brazo","balad","basar","bigas","bufon","bujia","batey","bombo","bohio","balar","breva","baria","bueno","balas","betel","bicho","baldo","bollo","bofar","balin","balsa","balon","bamba","bambu","bomba","balto","banal","banyo","banzo","bantu","banar","banil","barba","banos","barca","beata","bardo","borda","baron","barra","barre","barza","batik","beige","bella","bello","bemol","bemba","bidon","bilis","betun","borda","botar","boxeo","brama","brujo","bucal","budin","bucle","burdo","cabal","cable","cabos","cacao","cactus","caias","calco","caldo","calce","calas","calar","calma","calor","calvo","calza","campo","canal","canas","canes","caney","canje","canoa","canon","cansa","canto","canal","canas","canon","caobo","canos","capaz","caray","caras","cardo","carey","cargo","carie","carpa","caros","carta","carro","casar","casco","caspa","casto","causa","catre","cuaje","causa","cauto","cavar","cazar","cebar","cedro","ceder","cejas","ceibo","celar","celda","cenar","comic","censo","cenit","cerca","cerco","cesto","cetro","chapa","checo","chico","chips","choza","chuzo","cidra","ciclo","cifra","ciego","cinta","cinco","circo","clama","civil","clavo","clero","clima","colmo","combo","coche","comer","costa","costo","coxis","croar","cromo","cruce","crudo","cueva","curva","cutis","dados","dagas","damas","dando","dador","danes","danza","danar","danos","dardo","darse","datil","datos","deber","decai","debut","decir","dedal","dedos","dejar","delta","denso","dento","desde","deseo","deuda","diada","dicha","dicho","dicta","diera","dieta","digno","dijes","diodo","diosa","disco","divan","doble","dobla","docil","dogma","doler","dolor","domar","donar","donde","dones","dopar","dorar","dormi","dotar","dorso","dosis","drago","drama","dreno","ducha","ducto","dudar","duelo","duena","dulce","duque","durar","duros","ebano","ebrio","echar","ecuas","edema","edito","educa","efuso","egida","egeas","ejote","eleva","elfos","elige","elijo","elite","ellos","ellas","elles","elote","eluda","elude","emiti","emula","enano","encia","enema","enojo","entre","envia","eolio","epica","epoca","equis","ergui","ergos","eriza","erigi","errar","error","espia","esqui","estar","estoy","etano","etica","etimo","etnia","evado","evito","evoco","exige","exito","exodo","facha","facil","facto","faena","fagot","fajar","falaz","falco","falda","falla","falso","falta","fango","feria","farra","farol","farsa","fasto","fatal","fauna","favor","fecha","feliz","felpa","femur","fenix","feroz","ferry","feudo","fibra","fiare","ficha","fideo","fiera","fijar","filas","filme","final","finca","fingi","fines","firma","flash","flama","flaco","fleco","flojo","flora","flota","flema","fluia","flujo","fobia","fogon","folio","force","forja","forma","fosil","foton","frase","friso","freir","freno","fresa","fruta","fuego","fuera","funda","furor","fusil","fusta","gaban","gafas","gaita","gajos","galan","galas","gales","galon","ganar","garra","gases","gasto","gatea","gemas","gemir","genio","gente","gesta","girar","giros","glase","gleba","grifo","globo","gnomo","gozar","golpe","gordo","gorro","gotas","goteo","gozar","grabe","grada","grado","grafo","grama","gramo","grano","grapa","grasa","grato","grave","greca","grial","grito","grill","grima","gripe","grito","grune","grumo","grupo","gruta","guapa","guayo","gueto","guiar","guino","gusto","habas","haber","habil","habla","hacer","hacha","hacia","halar","halla","hampa","harto","hasta","hazlo","hebra","hecho","heces","hedor","helar","helio","henar","herir","heroe","herro","hervi","hiato","hidra","hielo","higos","hilar","himno","hindu","hindi","hiper","hobby","hojas","hogar","hondo","hongo","honro","horno","horas","hotel","hueco","huele","hueso","huevo","huida","humor","hurto","ibero","icono","ideal","ideas","idolo","igneo","igual","ilesa","iluso","imana","imita","impar","impio","ingle","infla","insto","inter","iones","infra","irias","istmo","italo","izara","izare","izote","jadea","jacta","jabon","jagua","jaiba","jalea","jalon","jamar","jaque","jarca","jarra","jarro","jauda","jaula","javas","jayan","jedar","jefas","jerez","jolin","jopar","joven","joyas","juicio","judio","juego","juega","jugar","junio","jumar","junta","junco","jurar","justo","juzga","kanji","kappa","karma","karts","kayak","kefir","kilos","kirie","koine","kurda","kurdo","labio","labro","lacia","labor","lacta","ladea","lados","ladra","laico","lamer","lanas","lance","lando","lanza","lanzo","lapos","lapso","lapiz","largo","larva","lares","latas","latex","latir","laton","leuda","lavar","laxos","lazar","lease","leche","legal","legar","logro","legia","lejos","lento","lenar","lenos","lepra","letal","levar","leyes","leves","liada","liana","liber","libra","libre","libro","licua","liceo","licor","lider","lidio","ligar","light","lijar","limon","limas","linda","linea","linio","lirio","lista","listo","litro","llama","llaga","llave","llego","llena","lleve","llora","local","logro","logos","lucha","lucid","lucro","luego","lugar","lunar","lunes","lupus","luxar","luzco","madre","mafia","magma","magna","magra","magia","majal","malos","malla","mamas","mamas","mamey","mamon","momia","manar","manda","mando","menea","manga","mango","mania","manos","manta","mansa","manas","maori","mapeo","marca","marea","marzo","marte","matar","matiz","mecer","mecha","media","medir","medio","megas","melon","memes","manso","menso","menos","mente","menta","mutar","merco","merey","metal","metas","meter","metro","micro","minar","miope","mioma","mirar","misil","mismo","mixto","modem","mofar","mohin","molar","molde","momia","mundo","monje","monja","moral","morir","motin","mover","mucho","mueca","muela","mujer","mundo","mutuo","nabos","nacar","nacer","negro","nadar","nadir","nadie","nafta","naipe","nardo","nariz","narra","nasal","natio","naval","naves","nebli","necio","negar","nevar","nexos","nicho","niega","nieve","nuevo","nilon","nieto","nieta","ninfo","ninez","nivel","nobel","noche","nitro","nodos","nogal","norma","norte","notar","nubes","nubil","nuble","nuero","nuevo","nueve","noras","nongo","naque","necas","natos","natas","napas","nango","nanga","noqui","names","najos","nonga","nonez","nocas","nonos","nique","nipes","ninga","obeso","oblea","obito","oboes","obrar","obten","obvio","ocaso","octal","ocumo","ocupa","odiar","oeste","odres","ohmio","oible","oidos","ojala","ojear","ojera","olivo","omega","omino","omiso","omita","ondas","ondea","onoto","opaco","opera","opera","opino","opone","optar","orcos","orden","orear","oreja","orfos","orina","orion","oruga","osada","ostia","otono","ovalo","otras","ovulo","oxido","ozono","pacto","padre","pagar","paila","pajar","palco","polea","palca","palma","panal","panel","papal","papas","papas","papel","parar","pardo","pared","parir","parla","parto","parva","parte","pasar","pasas","paseo","pasmo","pasos","pasto","patas","patin","patio","pausa","pauta","peaje","pecar","pecho","pedal","pedir","pegar","peine","penal","penar","penca","penas","penon","peque","perno","pesar","pesca","pezon","piano","picar","pecas","picor","pilla","pinta","pinza","pinon","pique","pisar","pista","pitar","pixel","pizca","pivot","pizza","placa","place","plaga","plano","plata","plato","playa","plaza","plazo","plano","plomo","pluma","podar","poder","poeta","polca","polen","polea","polvo","poner","posar","poste","prado","prima","primo","prior","prisa","prosa","pudin","puber","pudor","pugna","pujar","pulir","pulso","pulse","punir","punta","punza","purga","puzle","quark","queco","queda","quedo","queja","quema","quemi","quena","quepo","quera","quien","quien","queso","quijo","quila","quipa","quina","quino","quino","quiza","quite","quito","rabia","rabas","rocio","radar","recio","radio","radon","ragua","rahez","raido","rioja","rajar","rajen","raigo","ralla","ramas","rampa","rango","rapar","rapaz","rapte","repta","rapto","rasar","rasco","rasca","rasgo","raspa","raspe","ratos","rauda","raudo","rayar","rayos","razon","remar","reata","recae","roble","recen","recai","recta","recia","regia","regla","regar","rehen","reino","reloj","renal","remos","renta","renir","reojo","resta","retar","reten","reuma","retro","reune","reuso","reves","rezar","riego","rifar","rifle","rigor","rimas","rinde","ruina","rival","ritmo","robar","roble","robot","rodar","rodeo","rogar","roido","rollo","rombo","rompe","ronca","rosca","rubor","rubro","rugir","rumba","rumie","saber","sacar","sabio","sabor","sacia","sacro","sagaz","sajon","salar","salda","solde","saldo","salir","salio","salle","salme","salmo","salsa","salon","salta","salte","selva","salve","saman","salto","salud","salve","salvo","samba","samia","sanar","sanco","santa","santo","sarda","sarna","saque","satin","sauce","sardo","sauco","sauna","savia","sazon","secar","secta","secua","sedal","sedar","segar","segun","segur","sello","senda","senil","senal","senor","sepia","septo","seres","serio","sexto","sigla","siglo","silla","simil","sismo","sitio","sobre","socas","socia","socio","sodio","solar","somos","sonar","sonar","soplo","sordo","sorbo","suave","sucio","sudar","suelo","suero","sufre","sumar","super","sutil","tabla","tacho","tacon","tacto","tacos","tahur","taima","taino","taita","tajar","talar","tajos","talco","tales","talla","talon","talud","tamiz","tanga","tango","tanda","tanta","tanto","tanar","tapar","taran","tarde","tardo","tarea","tarot","tasar","tasca","tarro","tasco","tatuo","taxon","tazar","tauro","techo","tecla","tedio","tejas","tejer","telar","telon","temas","temer","temor","tempo","tener","tendi","tenis","tenor","tenso","terco","temor","termo","terso","tesis","tetra","texto","tibio","tiara","tirso","tieso","tilde","tilin","timar","timon","tinto","tenir","tiple","tirar","tiron","titan","tocar","toldo","tomar","tomos","tonto","tunel","topar","toque","torax","tosco","toser","totem","traer","trama","trans","trapo","trato","trial","tribu","trina","tripa","triza","trozo","tropa","troto","trova","trono","truco","tumba","tupir","turbo","turno","tutor","ubres","ubico","ubica","ufana","ulema","ultra","ulula","umbra","uncen","unido","uncir","uncia","unete","ungir","union","untar","unzan","untos","urano","urdia","urbes","urdir","urgio","urjas","urico","urica","usado","usara","usare","usase","usias","usted","usual","usure","utero","uvula","vacio","vadea","vagar","vagon","video","valer","valla","valor","vamos","vapor","vanos","varar","vario","varia","varon","vasco","vater","vatio","vayan","vease","vedar","vejez","velar","velon","vence","vende","vengo","venia","venta","venus","veras","venia","verbo","verso","vermu","verti","viaje","vibra","vicio","viejo","vieja","vigia","vigor","villa","viral","visco","vital","vivaz","vivir","viudo","visto","viste","vocal","vodka","volar","votar","vuelo","watts","weber","xecas","xenon","xinca","xiote","xolas","xolos","yacer","yates","yelmo","yergo","yendo","yenes","yerba","yedra","yerma","yerna","yerno","yerra","yesca","yogui","yogur","yumbo","yodos","yunta","yezgo","zafar","zafio","zafia","zagas","zagal","zamba","zanco","zanja","zarpa","zarzo","zenda","zetas","zocas","zombi","zueco","zumba","zumbo","zupia","zunir","zurci","zurdo","zurda","zurza","jacob","maria","susan","oscar","laura","david","nadia","james","jesus","josue","javier","sarai","saray","aime","alexa","anais","lucas","adamo","agata","agnus","zoila","angel","antia","berta","vilma","bimba","juana","celia","cesar","dalia","nubia","lucia","sofia","lucas","daria","dario","mario","mauro","diana","frank","dimas","edipo","nicol","jolie","julio","ester","guido","paula","tania","linda","lucio","marco","pablo","ramon","sasha","romeo","cadiz","ibiza","argel","cuzco","papua","cauca","andes","alpes","sudan","congo","china","congo","ejion","judea","kenia","libia","moscu","petra","nepal","quito","saudi","vegas","siena","macao","suiza","nandu","arana","abeja","oveja","ameba","cebra","hiena","yegua","perro","perra","llama","cerdo","bagre","lemur","cobra","coqui","okapi","leona","tigre","lince","burro","burra","gallo","saino","zorro","zorra","corzo","erizo","ganso","piton","simio","ostra","tucan","ovino","mamut","garza","koala","mirlo","morsa","panda","larva","piojo","potro","potra","corua","mosca","mosco","cabra","cebra","danta","coati","pulpo","tenia","chivo")
    val randNum = Random.nextInt(1, 1661)
    //Desglosar la palabra escogida
    val paraula = diccionari[randNum].chunked(1)

    println("---- Wordle! ----")
    do {
        //Imprimeix taula
        for (row in taula) {
            if (row != null) {
                for (col in row){
                    print( "$col ")
                }
            }
            println()
        }
        println("-----------------")
        //Introducció de paraules i converteix a array
        println("Introduce una palabra")
        val userInput = reader.next()
        val paraulaUser = userInput.chunked(1)
        //Comprova llargada de la paraula
        val llargada = userInput.length
        if (llargada != 5){
            println("La palabra debe contener 5 carácteres! Prueba otra vez.")
        }
        if (!diccionari.contains(userInput)){
            println("La palabra debe estar en el diccionario!")
        }
        //Estableix el valor de la paraula introduida a la fila corresponent.
        else{
            for(i in paraulaUser.indices){
                //Comprovació de cada lletra de la paraula introduida amb la paraula final
                //TODO comprovar si la lletra surt dues vegades per posar una en groc i l'altre en vermell.
                for(j in paraula.indices){
                    //Lletra correcte
                    if(paraulaUser[i] == paraula[i]){
                        taula[intents-1][i] = verd + paraulaUser[i] + reset
                    }
                    //Correcte pero a un lloc diferent (si la paraula del diccionari conté la lletra del input del usuari)
                    else if (paraula.contains(paraulaUser[i])){
                        taula[intents-1][i] = groc + paraulaUser[i] + reset
                    }
                    //Incorrecte
                    else{
                        taula[intents-1][i] = vermell + paraulaUser[i] + reset
                    }
                }
            }
            intents += 1
        }
        //Comprova si la paraula del diccionari es igual a la paraula introduida per el usuari
        if (paraulaUser == paraula){
            println("Has ganado!!")
            intents = 7
        }
    } while (intents <= 6)
    //Imprimeix taula final
    println("Aqui tienes la tabla final. Compartela con tus amigos!")
    //Imprimeix taula
    for (row in taula) {
        if (row != null) {
            for (col in row){
                print("$col ")
            }
        }
        println()
    }
    //Imprimeix paraula final
    print("La palabra final era: ")
    for(i in paraula.indices){
        print(paraula[i])
    }
}